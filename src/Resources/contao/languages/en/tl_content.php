<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_teasersimple'] = ['Teaser Simple', 'Full-Width Text Block with backgroundcolor.'];

$GLOBALS['TL_LANG']['tl_content']['dse_secondline']  =
    ['Headline (Line 2)', 'Here you can add a second line to the headline.'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle']  = ['CTA Button Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']   = ['Background Color', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Background Image', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_image']     = ['Pop-Out Image', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_imageSize'] = ['Pop-Out Image size', ''];
