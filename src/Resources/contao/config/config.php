<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_teasersimple'] =
    'Dse\\ElementsBundle\\ElementTeasersimple\\Element\\ContentDseTeasersimple';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_teasersimple'] = 'bundles/dseelementteasersimple/css/fe_ce_dse_teasersimple.css';

