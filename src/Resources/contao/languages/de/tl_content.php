<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_teasersimple'] = ['Teaser Simpel', 'Full-Width Text Block mit Hintergrundfarbe.'];

$GLOBALS['TL_LANG']['tl_content']['secondline']  =
    ['Überschrift (Zeile 2)', 'Fügt einer Überschrift eine zweite Zeile hinzu'];
$GLOBALS['TL_LANG']['tl_content']['subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle']  = ['CTA Button Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']   = ['Hintergrund Farbe', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Hintergrund Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_image']     = ['Pop-Out Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_imageSize'] = ['Pop-Out Bild Größe', ''];
