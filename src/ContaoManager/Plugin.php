<?php

namespace Dse\ElementsBundle\ElementTeasersimple\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementTeasersimple;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementTeasersimple\DseElementTeasersimple::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
